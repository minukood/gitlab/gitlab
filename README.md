# GitLab

A collection of links to GitLab documentation, concepts and tools related to GitLab.  
Useful when building GitLab pipelines.

## CI/CD Pipelines

Documentation: https://docs.gitlab.com/ee/ci/yaml/

Built in templates: https://gitlab.com/gitlab-org/gitlab/tree/master/lib/gitlab/ci/templates

### Variables

Documentation: https://docs.gitlab.com/ee/ci/variables/

### Job - YAML Anchors

Documentation: https://docs.gitlab.com/ee/ci/yaml/#anchors

### Job - extends

Documentation: https://docs.gitlab.com/ee/ci/yaml/#extends

### Pipelines - include

Documentation: https://docs.gitlab.com/ee/ci/yaml/#include

## Tools

Tools can use the GitLab API to perform actions on the GitLab environment.

### GitLab API

Documentation: https://docs.gitlab.com/ee/api/

### Renovate

> Automated dependency updates. Multi-platform and multi-language.

Repository: https://gitlab.com/minukood/gitlab/gitlab-renovate

### Pipeline-trigger

> Pipeline-trigger allows you to trigger and wait for the results of another GitLab pipeline

Repository: https://gitlab.com/finestructure/pipeline-trigger

## Changes
